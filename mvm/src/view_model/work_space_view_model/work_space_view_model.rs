use std::os::raw::c_char;
use std::ffi::CStr;
use crate::model::converter::iconverter::IConverter;
use crate::model::converter::converter::Converter;
use std::thread;
use std::sync::{Arc, Mutex};
use std::sync::mpsc;
use std::sync::mpsc::Sender;
use crate::model::converter::converter_ffi::{IConverterFFI, ConverterFFI};


pub struct WorkSpaceViewModel {
    m_converter: Arc<Mutex<dyn IConverter>>,
    m_tx: Sender<String>,
}

impl WorkSpaceViewModel {
    fn new(converter: Arc<Mutex<dyn IConverter>>) -> WorkSpaceViewModel {
        let (tx, rx) = mpsc::channel();

        let work_space_view_model = WorkSpaceViewModel {
            m_converter: converter,
            m_tx: tx,
        };

        let converter_clone = work_space_view_model.m_converter.clone();
        thread::spawn(move || {
            loop {
                //println!("sfsdf");
                converter_clone.lock().unwrap().convert_to_html(&rx);
            }
        });

        work_space_view_model
    }

    fn text_changed(&mut self, text: String) {
        self.m_tx.send(text).unwrap();
    }
}

#[no_mangle]
pub extern fn work_space_view_model_new() -> *mut WorkSpaceViewModel {
    let ffi: Arc<Mutex<dyn IConverterFFI>> = Arc::new(Mutex::new(ConverterFFI::new()));
    let converter = Arc::new(Mutex::new(Converter::new(ffi)));

    Box::into_raw(Box::new(WorkSpaceViewModel::new(converter)))
}

#[no_mangle]
pub extern fn work_space_view_model_text_changed(ptr: *mut WorkSpaceViewModel, s: *const c_char) {
    let work_space_view_model = unsafe {
        assert!(!ptr.is_null());
        &mut *ptr
    };

    let c_str = unsafe {
        assert!(!s.is_null());

        CStr::from_ptr(s)
    };
    let bla = String::from(c_str.to_str().unwrap());

    work_space_view_model.text_changed(bla);
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::model::converter::iconverter::MockIConverter;
    use mockall::predicate::{eq, ge};
    use mockall::predicate::*;
    use std::sync::mpsc::Receiver;
    use mockall::predicate;


//    #[test]
//    fn text_changed() {
//        let mut mock_ffi = MockIWorkSpaceViewModelFFI::new();
//        //mock_ffi.expect_work_space_view_model_request_text().times(1).return_const(());
//        let mut mock_converter = MockIConverter::new();
//        mock_converter.expect_convert_to_html().times(1).return_const(());
//
//        let mut work_space_view_model = WorkSpaceViewModel::new(Arc::new(Mutex::new(mock_ffi)),
//                                                                Arc::new(Mutex::new(mock_converter)));
//
//        work_space_view_model.text_changed(String::from("some_text"));
//
//        thread::sleep(Duration::from_millis(200));
//    }
}