use crate::model::converter::iconverter::IConverter;
use std::sync::{Mutex, Arc};
use std::sync::mpsc::Receiver;
use std::io::{Write, Read};
use std::error::Error;
use std::process::{Command, Stdio};
use std::os::windows::process::CommandExt;
use crate::model::converter::converter_ffi::IConverterFFI;


pub(crate) struct Converter {
    m_ffi: Arc<Mutex<dyn IConverterFFI>>,
    m_previous_text: String,
}

impl Converter {
    pub(crate) fn new(ffi: Arc<Mutex<dyn IConverterFFI>>) -> Converter {
        Converter {
            m_ffi: ffi,
            m_previous_text: String::new(),
        }
    }

    fn perform_conversion(&self, text: &str) -> String {
        let process = match Command::new("C:\\Ruby25-x64\\bin\\asciidoctor.bat").arg("-")
            .creation_flags(0x08000000)
            .stdin(Stdio::piped())
            .stdout(Stdio::piped())
            .spawn() {
            Err(why) => panic!("couldn't spawn process: {}", why.description()),
            Ok(process) => process,
        };

        match process.stdin.unwrap().write_all(text.as_bytes()) {
            Err(why) => panic!("Couldn't write to process stdin: {}.", why.description()),
            Ok(_) => (),
        }

        let mut res = String::new();
        match process.stdout.unwrap().read_to_string(&mut res) {
            Err(why) => panic!("Couldn't read process stdout: {}.", why.description()),
            Ok(_) => (),
        }

        res
    }
}

impl IConverter for Converter {
    fn convert_to_html(&mut self, rx: &Receiver<String>) {
        let new_text = rx.recv().unwrap();

        if new_text != self.m_previous_text.to_string() {
            self.m_ffi.lock().unwrap().work_space_view_model_notify_new_status("Working...");

            let res = self.perform_conversion(&new_text);

            self.m_ffi.lock().unwrap().work_space_view_model_notify_new_html(res.as_str());
            self.m_ffi.lock().unwrap().work_space_view_model_notify_new_status("Idle...");
        }

        self.m_ffi.lock().unwrap().work_space_view_model_request_text();

        self.m_previous_text = new_text;
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::model::converter::iconverter::MockIConverter;
    use mockall::predicate::{eq, ge};
    use mockall::predicate::*;
    use std::sync::mpsc::Receiver;
    use mockall::predicate;
    use std::sync::mpsc;
    use crate::model::converter::converter_ffi::ConverterFFI;
    use crate::model::converter::converter_ffi::IConverterFFI;
    use crate::model::converter::converter_ffi::MockIConverterFFI;


    #[test]
    fn convert_to_html_basic_flow()
    {
        let mut mock_ffi = MockIConverterFFI::new();
        mock_ffi.expect_work_space_view_model_notify_new_status().times(1).with(eq("Working...")).return_const(());
        mock_ffi.expect_work_space_view_model_notify_new_html().times(1).return_const(());
        mock_ffi.expect_work_space_view_model_notify_new_status().times(1).with(eq("Idle...")).return_const(());
        mock_ffi.expect_work_space_view_model_request_text().times(1).return_const(());

        let mut converter = Converter::new(Arc::new(Mutex::new(mock_ffi)));

        let (tx, rx) = mpsc::channel();

        tx.send("some_asciidoctor_text".to_string());
        converter.convert_to_html(&rx);
    }

    #[test]
    fn convert_to_html_same_text()
    {
        let (tx, rx) = mpsc::channel();
        let mut mock_ffi = MockIConverterFFI::new();
        mock_ffi.expect_work_space_view_model_notify_new_status().times(1).with(eq("Working...")).return_const(());
        mock_ffi.expect_work_space_view_model_notify_new_html().times(1).return_const(());
        mock_ffi.expect_work_space_view_model_notify_new_status().times(1).with(eq("Idle...")).return_const(());
        mock_ffi.expect_work_space_view_model_request_text().times(2).return_const(());
        //let mock_ffi_arc = Arc::new(Mutex::new(mock_ffi));

        let mut converter = Converter::new(Arc::new(Mutex::new(mock_ffi)));

        tx.send("some_asciidoctor_text".to_string());
        converter.convert_to_html(&rx);

        tx.send("some_asciidoctor_text".to_string());
        converter.convert_to_html(&rx);
    }
}