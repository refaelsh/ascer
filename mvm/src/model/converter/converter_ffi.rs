use std::os::raw::c_char;
use std::ffi::CString;
use mockall::*;
use mockall::predicate::*;


#[automock]
pub(crate) trait IConverterFFI: Send + Sync {
    fn work_space_view_model_notify_new_html(&self, html: &str);
    fn work_space_view_model_notify_new_status(&self, status: &str);
    fn work_space_view_model_request_text(&self);
}

pub struct ConverterFFI {}

impl ConverterFFI {
    pub(crate) fn new() -> ConverterFFI {
        ConverterFFI {}
    }
}

impl IConverterFFI for ConverterFFI {
    fn work_space_view_model_notify_new_html(&self, html: &str) {
        let s = CString::new(html).unwrap();
        unsafe {
            work_space_view_model_notify_new_html(s.as_ptr());
        }
    }

    fn work_space_view_model_notify_new_status(&self, status: &str) {
        let s = CString::new(status).unwrap();
        unsafe {
            work_space_view_model_notify_new_status(s.as_ptr());
        }
    }

    fn work_space_view_model_request_text(&self) {
        unsafe {
            work_space_view_model_request_text();
        }
    }
}

extern "C" {
    fn work_space_view_model_notify_new_html(s: *const c_char);
}

extern "C" {
    fn work_space_view_model_notify_new_status(s: *const c_char);
}

extern "C" {
    fn work_space_view_model_request_text();
}