use mockall::*;
use mockall::predicate::*;
use std::sync::mpsc::Receiver;


#[automock]
pub(crate) trait IConverter: Send + Sync {
    fn convert_to_html(&mut self, rx: &Receiver<String>);
}