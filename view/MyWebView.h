#pragma once
#include "wx/webview.h"
#include "wx/panel.h"


class MyWebView : public wxEvtHandler
{
public:
	MyWebView(wxWindow* parent);
	~MyWebView();

	wxWebView* Get_web_view() const;

private:

	wxWindow* m_parent{};
	wxWebView* m_web_view{};
	
	void Subscribe_to_observables();

	void On_null(wxCommandEvent& event);
};
