#include "MyScintilla.h"


MyScintilla::MyScintilla(wxWindow* parent, const wxWindowID id, const wxPoint& pos, const wxSize& size, const long style, const wxString& name)
	: wxStyledTextCtrl(parent, id, pos, size, style, name)
{
	Connect_events();

	Subscribe_to_observables();
}

MyScintilla::~MyScintilla()
{
	//this->Disconnect(wxEVT_STC_CHARADDED, wxKeyEventHandler(MyScintilla::On_change), nullptr, this);
}

void MyScintilla::Connect_events()
{
	//this->Connect(wxEVT_STC_CHARADDED, wxKeyEventHandler(MyScintilla::On_change), nullptr, this);
}

void MyScintilla::Subscribe_to_observables()
{
	request_text.subscribe([=]()
	{
		work_space_view_model_text_changed(m_view_model, this->GetText().ToStdString().c_str());
	});
}

void MyScintilla::Set_view_model(WORK_SPACE_VIEW_MODEL_T* view_model)
{
	m_view_model = view_model;

	// This will jump start the conversion loop.
	work_space_view_model_text_changed(m_view_model, this->GetText().ToStdString().c_str());
}
