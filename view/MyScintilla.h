#pragma once
#include "wx/stc/stc.h"
#include "WorkSpaceViewModelRustFFI.h"


class MyScintilla final : public wxStyledTextCtrl
{
public:
	explicit MyScintilla(wxWindow* parent, wxWindowID id = wxID_ANY, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = 0,
	                     const wxString& name = wxEmptyString);
	~MyScintilla();

	void Set_view_model(WORK_SPACE_VIEW_MODEL_T* view_model);

private:
	WORK_SPACE_VIEW_MODEL_T* m_view_model{};
	
	void Connect_events();

	void Subscribe_to_observables();
	
	//void On_change(wxKeyEvent& event);
};
