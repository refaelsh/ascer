#include "MyWebView.h"
#include "wx/sizer.h"
#include "wx/stattext.h"
#include "WorkSpaceViewModelRustFFI.h"


MyWebView::MyWebView(wxWindow* parent) : m_parent(parent)
{
	m_web_view = wxWebView::New(parent, wxID_ANY/*, "www.google.com"*/);

	Subscribe_to_observables();

	m_web_view->Connect(wxEVT_NULL, wxCommandEventHandler(MyWebView::On_null), nullptr, this);
}

MyWebView::~MyWebView()
{
	m_web_view->Disconnect(wxEVT_NULL, wxCommandEventHandler(MyWebView::On_null), nullptr, this);
}

void MyWebView::Subscribe_to_observables()
{
	new_html.subscribe([=](const char* html)
	{
		auto* evt = new wxCommandEvent;
		evt->SetString(html);
		
		m_web_view->GetEventHandler()->QueueEvent(evt);
	});
}

// ReSharper disable once CppMemberFunctionMayBeConst
void MyWebView::On_null(wxCommandEvent& event)
{
	const wxString html = event.GetString();
	
	m_web_view->SetPage(html, "");
}

wxWebView* MyWebView::Get_web_view() const
{
	return m_web_view;
}
