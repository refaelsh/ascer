#pragma once
#include <wx/wx.h>
#include "MainFrame.h"
#include <filesystem>


class MyApp final : public wxApp
{
public:
	MyApp();

	bool OnInit() override;

private:
	MainFrame* m_main_frame;
};
