#pragma once
#pragma warning( push )
#pragma warning( disable : 4267)
#include "MainFrameBase.h"
#include "WorkSpaceViewModelRustFFI.h"
#include "MyWebView.h"
#pragma warning(pop)


class MainFrame final : public MainFrameBase
{
public:
	explicit MainFrame(wxWindow* parent);

private:
	WORK_SPACE_VIEW_MODEL_T* m_scintilla_view_model{};

	MyWebView* m_my_web_view{};

	void Create_view_models();

	void Pass_the_view_models() const;

	void General_gui_init();

	void Subscribe_to_observables();
};
