#include "MyApp.h"
#include <sstream>


MyApp::MyApp()
{
	m_main_frame = nullptr;
}

bool MyApp::OnInit()
{
	wxInitAllImageHandlers();
	m_main_frame = new MainFrame(nullptr);
	this->SetTopWindow(m_main_frame);

	return true;
}