#include "MainFrame.h"
#include "MyScintilla.h"


MainFrame::MainFrame(wxWindow* parent) : MainFrameBase(parent)
{
	General_gui_init();

	Create_view_models();

	Pass_the_view_models();

	Subscribe_to_observables();
}

void MainFrame::Create_view_models()
{
	m_scintilla_view_model = work_space_view_model_new();
}

void MainFrame::Pass_the_view_models() const
{
	m_scintilla->Set_view_model(m_scintilla_view_model);
}

void MainFrame::General_gui_init()
{
	this->wxTopLevelWindowMSW::Show();
	this->wxTopLevelWindowMSW::Maximize();

	m_my_web_view = new MyWebView(m_web_view_panel);
	m_web_view_sizer->Add(m_my_web_view->Get_web_view(), 1, wxEXPAND, 5);

	m_status_bar->SetStatusText("Idle...");
}

void MainFrame::Subscribe_to_observables()
{
	new_status.subscribe([=](const char* status)
	{
		m_status_bar->SetStatusText(status);
	});
}
