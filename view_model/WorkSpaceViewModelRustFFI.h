#pragma once
#include <observable/subject.hpp>


typedef struct work_space_view_model WORK_SPACE_VIEW_MODEL_T;

// ReSharper disable CppInconsistentNaming

inline observable::subject<void(const char*)> new_html;
extern "C" void work_space_view_model_notify_new_html(const char* html);

inline observable::subject<void(const char*)> new_status;
extern "C" void work_space_view_model_notify_new_status(const char* status);

inline observable::subject<void()> request_text;
extern "C" void work_space_view_model_request_text();

extern "C" WORK_SPACE_VIEW_MODEL_T* work_space_view_model_new();
extern "C" void work_space_view_model_text_changed(WORK_SPACE_VIEW_MODEL_T*, const char*);

// ReSharper restore CppInconsistentNaming

// // ReSharper disable CppInconsistentNaming
//
// extern "C" VIEW_MODEL_T* view_model_new(const char*);
// extern "C" void view_model_free(VIEW_MODEL_T*);
//
// extern "C" TREE_VIEW_MODEL_T* view_model_get_tree_view_model(VIEW_MODEL_T*);
// extern "C" TERMINATOR_VIEW_MODEL_T* view_model_get_terminator_view_model(VIEW_MODEL_T*);
// extern "C" STATUSER_VIEW_MODEL_T* view_model_get_statuser_view_model(VIEW_MODEL_T*);
// extern "C" FILER_VIEW_MODEL_T* view_model_get_filer_view_model(VIEW_MODEL_T*);
// extern "C" void view_model_keyboard_shortcut_was_pressed(VIEW_MODEL_T*, bool, unsigned int, bool);
//
// // ReSharper restore CppInconsistentNaming
