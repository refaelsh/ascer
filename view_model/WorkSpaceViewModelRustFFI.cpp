#include "WorkSpaceViewModelRustFFI.h"


// ReSharper disable CppInconsistentNaming

void work_space_view_model_notify_new_html(const char* html)
{
	new_html.notify(html);
}

void work_space_view_model_notify_new_status(const char* status)
{
	new_status.notify(status);
}

void work_space_view_model_request_text()
{
	request_text.notify();
}

// ReSharper restore CppInconsistentNaming
