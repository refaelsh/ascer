# Ascer

This is goanna be an [Asciidoctor](https://asciidoctor.org/) IDE. Written using the MVVM design pattern. The View will use the [wxWidgets GUI](https://www.wxwidgets.org/) framework in C++ (or maybe in native Rust using some binding to GTK/wxWidgets). The ViewModel and the Model will be done in Rust offcourse.
